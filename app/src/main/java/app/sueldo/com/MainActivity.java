package app.sueldo.com;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private EditText salary,years;
    private TextView txt_salary,txt_aument,txt_salary_fin;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        salary = (EditText)findViewById(R.id.salary);
        years = (EditText)findViewById(R.id.years);
        txt_salary = (TextView)findViewById(R.id.txt_salary);
        txt_aument = (TextView)findViewById(R.id.txt_aument);
        txt_salary_fin = (TextView)findViewById(R.id.txt_salary_final);
    }


    public void calcular(View view){
        float sala = 0;
        double salary_final,aument;
        int year = 0;
        String salarT = salary.getText().toString();
        String yearT = years.getText().toString();
        sala = Float.valueOf(salarT);
        year = Integer.valueOf(yearT);

        if(sala < 500 && year >=10){
            salary_final = sala * 1.20;
            aument = 0.20 *100;
        }else if(sala < 500 && year < 10 ){
            salary_final = sala * 1.05;
            aument = 0.05 * 100;
        }else{
            salary_final = sala;
            aument = 0;
        }
        String sl = String.valueOf(sala);
        String au = String.valueOf(aument);
        String slf = String.valueOf(df2.format(salary_final));
        txt_salary.setText("Sueldo Base: $" + sl);
        txt_aument.setText("Aumento: "+ au + "%");
        txt_salary_fin.setText("Salario a Pagar: $" + slf);
    }
}